package com.phreakingawesome.rest_tutorial;

import com.phreakingawesome.rest_tutorial.security.Crypto;
import com.phreakingawesome.rest_tutorial.security.Session;
import com.phreakingawesome.rest_tutorial.security.Token;
import org.apache.commons.codec.DecoderException;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.apache.commons.codec.binary.Hex;
import org.glassfish.jersey.server.ServerProperties;

import javax.ws.rs.core.UriBuilder;
import java.io.Console;
import java.sql.*;

public class Main {
  
  private String SQL_URL;
  private String SQL_port;
  private String SQL_uname;
  private String SQL_pass;
  private Connection conn = null;
  private Console console;
  
  public static final Main MAIN = new Main();
  
  public Main() {
  
  
  }
  
  public static void main(String[] args) {
    MAIN.console = System.console();
    if (MAIN.console == null) {
      System.out.println("Couldn't get Console instance");
      System.exit(0);
    }
    
    MAIN.setSQL_URL(MAIN.console.readLine("%-30s", "Enter your sql conn url:"));
    MAIN.setSQL_port(MAIN.console.readLine("%-30s", "Enter your sql conn port:"));
    MAIN.setSQL_uname(MAIN.console.readLine("%-30s","Enter your mysql username:"));
    MAIN.setSQL_pass(new String(MAIN.console.readPassword("%-30s","Enter your mysql password:")));
  
    
    HttpServer server = null;
    ResourceConfig rc = new ResourceConfig().packages("com.phreakingawesome.rest_tutorial.pages", "com.phreakingawesome.rest_tutorial.pages.models");
    rc.register(JacksonFeature.class);
    
    try {
      Class.forName("com.mysql.cj.jdbc.Driver");
      MAIN.conn = DriverManager.getConnection("jdbc:mysql://"+MAIN.getSQL_URL()+":"+MAIN.getSQL_port()+"/users", MAIN.getSQL_uname(), MAIN.getSQL_pass());
      MAIN.console.printf("Connected to sql server...%n");
      server = GrizzlyHttpServerFactory.createHttpServer(UriBuilder.fromUri("http://0.0.0.0/api").port(8080).build(), rc);
    } catch (ClassNotFoundException | SQLException e) {
      e.printStackTrace();
      System.exit(0);
    }
  }
  
  public Session checkPassword(String uname, String password, String ip) {
    try {
      PreparedStatement statement = conn.prepareStatement("SELECT email, salt, password from users where email = ?");
      statement.setString(1,uname);
      ResultSet results = statement.executeQuery();
      
      results.last();
      if (results.getRow() == 1) {
        String hashedPass = Hex.encodeHexString(Crypto.hashPassword(password.toCharArray(), Hex.decodeHex(results.getString("salt")), 10000, 512));
        if (hashedPass.equals(results.getString("password"))) {
          return Session.newSession(uname, ip);
        }
      }
      
    } catch (SQLException | DecoderException e) {
      e.printStackTrace();
    }
    return null;
  }
  
  public String getSQL_URL() {
    return SQL_URL;
  }
  
  public void setSQL_URL(String SQL_URL) {
    this.SQL_URL = SQL_URL;
  }
  
  public String getSQL_uname() {
    return SQL_uname;
  }
  
  public void setSQL_uname(String SQL_uname) {
    this.SQL_uname = SQL_uname;
  }
  
  public String getSQL_pass() {
    return SQL_pass;
  }
  
  public void setSQL_pass(String SQL_pass) {
    this.SQL_pass = SQL_pass;
  }
  
  public String getSQL_port() {
    return SQL_port;
  }
  
  public void setSQL_port(String SQL_port) {
    this.SQL_port = SQL_port;
  }
}
