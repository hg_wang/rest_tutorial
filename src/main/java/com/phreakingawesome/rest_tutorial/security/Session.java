package com.phreakingawesome.rest_tutorial.security;

public final class Session {
  public static long SESSION_TIMEOUT = 1200000;
  
  private final Token sessionID;
  private final String username;
  private final String ip;
  private final long creationTime;
  private long deltaUse;
  
  private Session(String username, String ip) {
    sessionID = Token.generateSessionToken();
    this.username = username;
    this.ip = ip;
    creationTime = System.currentTimeMillis();
    deltaUse = creationTime;
  }
  
  public static Session newSession(String name, String ip) {
    return new Session(name, ip);
  }
  
  public boolean verify(String id, String ip) {
    if (sessionID.validate(id) && this.ip.equalsIgnoreCase(ip) && deltaUse + SESSION_TIMEOUT > System.currentTimeMillis()) {
      deltaUse = System.currentTimeMillis();
      return true;
    }
    return false;
  }
  
  public Token getSessionID() {
    return sessionID;
  }
  
  public String getUsername() {
    return username;
  }
  
  public String getIp() {
    return ip;
  }
  
  public long getCreationTime() {
    return creationTime;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Session)) return false;
    
    Session session = (Session) o;
    
    return getCreationTime() == session.getCreationTime() && getSessionID().equals(session.getSessionID()) && getUsername().equals(session.getUsername()) && getIp().equals(session.getIp());
  }
  
  @Override
  public int hashCode() {
    return getSessionID().hashCode();
  }
}
